package info.hccis.sample.services;

import info.hccis.sample.dao.StudentDAO;
import info.hccis.sample.entity.Student;
import javax.jws.WebService;

@WebService(endpointInterface = "info.hccis.sample.services.StudentService")
public class StudentServiceImpl implements StudentService {


    public Student getStudent(int id) {
        Student student = new Student();
        student.setName("test");
        StudentDAO studentDAO = new StudentDAO();
        student = studentDAO.selectStudenById(id);
        return student;
    }
}
