package info.hccis.sample.dao;

import info.hccis.sample.entity.Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DAO class for student 
 *
 * @author bjmaclean
 * @since 20211001
 */
public class StudentDAO {

    private static ResultSet rs;
    private static Statement stmt = null;
    private static Connection conn = null;

    public StudentDAO() {

        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        String connectionString = rb.getString("spring.datasource.url");
        String userName = rb.getString("spring.datasource.username");
        String password = rb.getString("spring.datasource.password");

        //String URL = "jdbc:mysql://" + "localhost" + ":3306/" + "cis2232_sample";
        try {
            conn = DriverManager.getConnection(connectionString, userName, password);
        } catch (Exception e) {
            System.out.println("Error");
        }


    }

    /**
     * Select a student by id
     *
     * @since 20210924
     * @author BJM
     */
    public Student selectStudenById(int id) {
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            String query = "SELECT s.name, s.program from Student s WHERE s.studentId = "+id;
            System.out.println(query);
            rs = stmt.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }

        Student student = new Student();
        try {
            while (rs.next()) {
                student.setName(rs.getString(1));
                student.setProgram(rs.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return student;
    }

}
