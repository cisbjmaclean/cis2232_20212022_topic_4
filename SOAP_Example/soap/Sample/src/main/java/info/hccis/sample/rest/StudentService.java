package info.hccis.sample.rest;

import com.google.gson.Gson;
import info.hccis.sample.entity.Student;
import info.hccis.sample.repositories.StudentRepository;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/StudentService/student")
public class StudentService {

    private final StudentRepository _sr;

    @Autowired
    public StudentService(StudentRepository _sr) {
        this._sr = _sr;
    }
    @GET
    @Produces("application/json")
    public ArrayList<Student> getAll() {
        ArrayList<Student> students = (ArrayList<Student>) _sr.findAll();
        return students;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getStudentById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Student> student = _sr.findById(id);

        if (!student.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(student).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createStudent(String studentJson) 
    {        
        try{
            String temp = save(studentJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
////    
////    @DELETE
////    @Path("/{id}")
////    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
////        Optional<Booking> booking = br.findById(id);
////        if(booking != null) {
////            br.deleteById(id);
////            return Response.status(HttpURLConnection.HTTP_CREATED).build();
////        }
////        return Response.status(404).build();
////    }
////
//    @PUT
//    @Path("/{id}")
//    @Consumes("application/json")
//    @Produces("application/json")
//    public Response updateCamper(@PathParam("id") int id, String camperJson) throws URISyntaxException 
//    {
//
//        try{
//            String temp = save(camperJson);
//            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
//                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
//        }catch(AllAttributesNeededException aane){
//            return Response.status(400).entity(aane.getMessage()).build();
//        }
//
//    }
//
    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        Student student = gson.fromJson(json, Student.class);
        
//        if(camper.getFirstName() == null || camper.getFirstName().isEmpty()) {
//            throw new AllAttributesNeededException("Please provide all mandatory inputs");
//        }
 
        if(student.getStudentId() == null){
            student.setStudentId(0);
        }

        student = _sr.save(student);

        String temp = "";
        temp = gson.toJson(student);

        return temp;
        
        
    }
}
