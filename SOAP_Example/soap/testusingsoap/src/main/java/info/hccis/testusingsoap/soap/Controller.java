package info.hccis.testusingsoap.soap;

import javax.swing.JOptionPane;

/**
 * Test soap
 * @author bjmac
 * @since 20211115
 * 
 */
public class Controller {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
   
        StudentServiceImplService ssis = new StudentServiceImplService();
        StudentService ss = ssis.getStudentServiceImplPort();

        String idEntered = JOptionPane.showInputDialog("Enter id to lookup");
        Student student =  ss.getStudent(Integer.parseInt(idEntered));
        System.out.println(student.getName());
        JOptionPane.showMessageDialog(null, "The student's name is: "+student.getName()+" "+student.getProgram());

        
    }
    
}
