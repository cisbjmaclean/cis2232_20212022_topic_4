package info.hccis.starting.bo;

import info.hccis.util.UtilityRest;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.json.*;

/**
 *
 * @author bjmaclean
 * @since Nov 4, 2021
 */
public class ApiProcessor {

    public static final String URL = "https://api.funtranslations.com/translate/pirate.json?text=";

    public static String callApi(String english) {

        //https://stackoverflow.com/questions/10786042/java-url-encoding-of-query-string-parameters
        String json = UtilityRest.getJsonFromRest(URL + URLEncoder.encode(english));
        System.out.println("json returned=" + json);

        String translation = "";
        try {
            //Next need to get the contents from the json string
            //https://www.tutorialspoint.com/how-can-we-parse-a-nested-json-object-in-java
            JSONObject jsonObject = new JSONObject(json);
            JSONObject jsonObjectContents = jsonObject.getJSONObject("contents");
            translation = jsonObjectContents.getString("translated");
        } catch (Exception e) {
            translation = "Unavailable";
        }
        System.out.println("translated=" + translation);
        return translation;

    }

}
