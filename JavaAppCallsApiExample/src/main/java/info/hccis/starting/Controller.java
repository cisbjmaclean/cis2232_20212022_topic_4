package info.hccis.starting;

import info.hccis.starting.bo.ApiProcessor;
import info.hccis.util.CisUtility;
import java.util.Scanner;

/**
 *
 * @author bjmaclean
 * @since Oct 19, 2021
 */
public class Controller {

    public static void main(String[] args) {
        
        System.out.println("Welcome ("+CisUtility.getTodayString("yyyy-MM-dd")+")");
        String english = CisUtility.getInputString("Enter some phrase to be translated to Pirate:");
        String translated = ApiProcessor.callApi(english);
        System.out.println("In pirate: "+translated);
    }

}
