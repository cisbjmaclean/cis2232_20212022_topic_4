package cis.bo;

import cis.entity.Category;
import cis.entity.Clue;
import cis.entity.Player;
import cis.util.CisUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Represents a game
 *
 * @author bjmac
 * @since 4-May-2021
 */
public class Game {

    private ArrayList<Clue> clues = new ArrayList(); //All possible clues
    private HashMap<String, Category> categoriesMap = new HashMap();
    private Player[] players = null;

    private ArrayList<Clue> currentClues;  //Curent clues
    private Category currentCategory;
    private Clue currentClue;

    public Game() {
    }

    public Game(ArrayList clues, HashMap categoriesMap) {
        this.clues = clues;
        this.categoriesMap = categoriesMap;
    }

    /**
     * Play the game
     *
     * @since 20210514
     * @author BJM
     */
    public void play() {
        //Welcome the players by introducing each player from the players array
        System.out.println("Welcome to Jeopardy, here are today's players:");
        for (Player current : players) {
            current.display();
        }

        //Have the user specify which category to play this game or default will be picked
        currentCategory = getCategoryPickFromUser();

        System.out.println("This game we are playing: " + currentCategory.getTitle());
        CisUtility.getInputString("Press enter to continue...");

        //Show the clues for that category
        System.out.println("");
        currentClues = getCluesForCategory(currentCategory.getId()); //Load current clues

        do {
            showCurrentPoints();
            currentClue = getClueFromUser();
            processAnswerForClue(currentClue);
            currentClues.remove(currentClue);
        } while (!currentClues.isEmpty());

    }

    /**
     * Process the answer for a Clue. - This will require obtaining the player
     * that is answering and then their answer. - Show the answer. - Have the
     * user enter if the answer was right. - If the answer is right then add #
     * otherwise deduct $. - The clue can then be removed from the currentClues.
     *
     * @since 20210602
     * @author BJM
     */
    public void processAnswerForClue(Clue clue) {

        CisUtility.display("\nHere's the hint:\n", CisUtility.BLACK);
        CisUtility.display(clue.getQuestion(), CisUtility.GREEN);

        int playerIndex = 0;
        if (players.length > 1) {
            playerIndex = getIndexOfPlayerAnswering();
        }

        boolean correct = clue.processQuestion(players[playerIndex].getName());
        adjustScore(clue, correct, playerIndex);

    }

    /**
     * Adjust the score for a clue.
     *
     * @since 20210602
     * @author BJM
     */
    public void adjustScore(Clue clue, boolean correct, int playerIndex) {
        int value = clue.getValue();
        int currentScore = players[playerIndex].getScore();

        if (correct) {
            players[playerIndex].setScore(currentScore + value);
        } else {
            players[playerIndex].setScore(currentScore - value);
        }
    }

    /**
     * Determine the player to provide an answer - Ask which player is answering
     * - Validate that the name is a valid player name
     *
     * @return The index from players of the answering player
     * @since 20210602
     * @author BJM
     */
    public int getIndexOfPlayerAnswering() {
        //Show the players 
        String prompt = "Who is answering ( ";
        for (Player current : players) {
            prompt += current.getName() + " ";
        }
        prompt += "):";

        boolean valid = false;
        int indexFound = -1;

        do {

            String name = CisUtility.getInputString(prompt, CisUtility.GREEN);

            for (int index = 0; index < players.length; index++) {

                if (players[index].getName().equals(name)) {
                    valid = true;
                    indexFound = index;
                    break;
                }
            }
        } while (!valid);
        return indexFound;
    }

    /**
     * Have the user enter the dollar value to pick a question.
     *
     * @return The Clue as specified by the user
     * @since 20210526
     * @author BJM
     */
    public Clue getClueFromUser() {
        //Show the clues
        showCurrentCluesDollarValues();
        currentClue = null;

        do {

            int dollarValue = CisUtility.getInputInt("Enter $ value");
            Clue dollarValueClue = new Clue(currentCategory.getId(), dollarValue);
            int indexFound = currentClues.indexOf(dollarValueClue);

            if (indexFound >= 0) {
                currentClue = currentClues.get(indexFound);
            } else {
                System.out.println("Not a valid $ value");
            }
        } while (currentClue == null);

        //Return the current clue.  
        return currentClue;
    }

    /**
     * This method should return a random category from the categoriesMap.
     *
     * @return A random category
     * @since 20210517
     * @author JW/BJ
     */
    public Category getRandomCategory() {

        Collection aCollection = categoriesMap.values();
        ArrayList<Category> allTheCategories = new ArrayList<Category>(aCollection);

        int numberOfCategories = allTheCategories.size();
        int randomNumber = CisUtility.getRandom(numberOfCategories);
        int randomIndex = randomNumber - 1;

        Category theRandomCategory = allTheCategories.get(randomIndex);
        return theRandomCategory;
    }

    /**
     * setup the players.
     *
     * @since 20210514
     * @author BJM
     */
    public void setupPlayers() {
        CisUtility.display("Setup Players");
        int numPlayers = 0;
        //DONE Ask how many players
        do {
            try {
                numPlayers = CisUtility.getInputInt("How many players?");
            } catch (Exception e) {
                System.out.println("Invalid entry. Please try again.");
            }
        } while (numPlayers == 0);
        //Call the createPlayers method
        createPlayers(numPlayers);

    }

    /**
     * This method will populate an array of n size and invoke the
     * getInformation method on each
     *
     * @param numPlayers the local variable collected from processMenuOption
     * @since 2021-05-13
     * @author mdw
     */
    private void createPlayers(int numPlayers) {
        System.out.println("Creating " + numPlayers + " players.");

        players = new Player[numPlayers];

        for (int i = 0; i < numPlayers; i++) {
            players[i] = new Player();
            players[i].getInformation();
        }
    }

    /**
     * Show the current clues
     *
     * @since 20210518
     * @author BJM
     */
    public void showCurrentClues() {

        for (Clue current : currentClues) {
            current.displayAnswer();
        }
    }

    /**
     * Show the clues
     *
     * @since 20210518
     * @author BJM
     */
    public void showCurrentCluesDollarValues() {
        System.out.println("Here are the values");
        for (Clue current : currentClues) {
            System.out.println("$" + current.getValue());
        }
    }

    /**
     * Show the clues for an entire category
     *
     * @since 20210518
     * @author BJM
     */
    public void showCluesDollarValuesForCategory(Category category) {
        System.out.println("Here are the values (" + category.getTitle() + ")");
        for (Clue current : getCluesForCategory(category.getId())) {
            System.out.println("$" + current.getValue());
        }
    }

    /**
     * Show the clues
     *
     * @since 20210518
     * @author BJM
     */
    public void showCluesForCategory(Category category) {
        System.out.println("Here are the clues (" + category.getTitle() + ")");
        for (Clue current : getCluesForCategory(category.getId())) {
            current.displayAnswer();
        }
    }

    /**
     * Put in the category id (int) and a list of possible clues will be
     * displayed
     *
     * @since 2021-05-13
     * @author jwhittaker
     */
    public ArrayList<Clue> getCluesForCategory(int category) {
        //re-initialize the new arraylist
        currentClues = new ArrayList();

        for (Clue clue : clues) {
            if (clue.getCategory_id() == category) {
                currentClues.add(clue);
            }
        }
        return currentClues;
    }

//    public ArrayList<Clue> getClues() {
//        return clues;
//    }
//
//    public void setClues(ArrayList<Clue> clues) {
//        this.clues = clues;
//    }
//
//    public HashMap<String, Category> getCategoriesMap() {
//        return categoriesMap;
//    }
//
//    public void setCategoriesMap(HashMap<String, Category> categoriesMap) {
//        this.categoriesMap = categoriesMap;
//    }
    /**
     * Build a prompt based on the categories and have the user specify which
     * category that they want.
     *
     * @since 2021-05-06
     * @author BJM
     */
    public Category getCategoryPickFromUser() {

        //Create a list of the categories so it can be sorted.  
        ArrayList<Category> categoriesList = new ArrayList(categoriesMap.values());
        Collections.sort(categoriesList);

        //Loop through each category to build the prompt
        System.out.println("Available categories");
        for (Category current : categoriesList) {
            System.out.println(current.getId() + ") " + current.getTitle());
        }
        int choice = CisUtility.getInputInt("Choice (Enter 0 for random): ");

        currentCategory = categoriesMap.get("" + choice);
        
        if (currentCategory == null) {
            System.out.println("Picking a random category");
            currentCategory = getRandomCategory();
        }

        return currentCategory;

    }

    /**
     * Builds a scoreboard type output with player names and points
     *
     * @since 2021-05-30
     * @author mdw
     */
    public void showCurrentPoints() {
        StringBuilder output = new StringBuilder();
        output.append("\n------------------------------\n"
                + "Name\t\t\tScore\n"
                + "------------------------------\n");
        //DONE needs logic to loop through the array of players and insert a line for each
        try {
            for (int i = 0; i < players.length; i++) {
                //Would like to have a mode ideal solution that avoids concatenation and newline/tab
                output.append(players[i].getName() + "\t\t\t" + players[i].getScore() + "\n");
            }
        } catch (NullPointerException ne) {
            System.out.println("No players");
        }
        CisUtility.display(output.toString());
    }

}
