package cis.util;

import cis.entity.Category;
import cis.entity.Clue;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;

/**
 * Utility methods to help setup categories and clues for a Jeopardy came. This
 * class will hide some of the complexity from accessing an online api which is
 * used to provide the game details.
 *
 * @author bjmac
 * @since 27-Apr-2021
 */
public class UtilJeopardy {

    private static ArrayList<Clue> clues = new ArrayList();
    private static HashMap<String, Category> categoriesMap = new HashMap();

    private static final String URL_STRING = "http://jservice.io/api/clues";

    public static String getAll() {
        return UtilityRest.getJsonFromRest(URL_STRING);
    }

    /**
     * This method will access the api and setup the categories and clues for
     * the Jeopardy game.
     *
     * @since 20210427
     * @author BJM
     */
    public static void setup() {

        clues.clear();

        String jsonReturned = UtilJeopardy.getAll();

        //**************************************************************
        //Based on the json string passed back, loop through each json
        //object which is a json string in an array of json strings.
        //*************************************************************
        JSONArray jsonArray = new JSONArray(jsonReturned);
        //**************************************************************
        //For each json object in the array, show the first and last names
        //**************************************************************
        System.out.println("Here are the rows");
        Gson gson = new Gson();
        for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
            Clue current = gson.fromJson(jsonArray.getJSONObject(currentIndex).toString(), Clue.class);

            //Issue#17
            //Add some validation on what's added.  The api is giving some invalid clues back.
            //If the value is 0 do not add to the collection of clues.
            if (current.getValue() != 0) {
                clues.add(current);
                Category category = current.getCategory();
                categoriesMap.put(String.valueOf(category.getId()), category);
            }
        }

        System.out.println("Clues loaded (" + clues.size() + "):");
        for (Clue current : clues) {
            current.display();
        }

        System.out.println("Categories loaded (" + categoriesMap.size() + "):");
        for (Category current : categoriesMap.values()) {
            current.display();
        }

    }

    public static ArrayList<Clue> getClues() {
        return clues;
    }

    public static HashMap<String, Category> getCategoriesMap() {
        return categoriesMap;
    }

}
