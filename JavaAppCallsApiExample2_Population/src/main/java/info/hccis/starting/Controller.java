package info.hccis.starting;

import info.hccis.starting.bo.ApiProcessor;
import info.hccis.util.CisUtility;
import java.util.Scanner;

/**
 *
 * @author bjmaclean
 * @since Oct 19, 2021
 */
public class Controller {

    public static void main(String[] args) {

        System.out.println("Welcome (" + CisUtility.getTodayString("yyyy-MM-dd") + ")");
        String year = CisUtility.getInputString("What year?");
        int population = ApiProcessor.callApi(year);
        if (population > 0) {
            System.out.println("The population in " + year + " was " + population);
        } else {
            System.out.println("Could not find populaton for " + 2015);
        }
    }

}
