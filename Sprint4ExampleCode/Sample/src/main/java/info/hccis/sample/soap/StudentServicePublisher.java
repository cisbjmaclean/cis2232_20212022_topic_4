package info.hccis.sample.soap;

import javax.xml.ws.Endpoint;

public class StudentServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/studentservice", 
          new StudentServiceImpl());
    }
}    
