package info.hccis.sample.soap;

import info.hccis.sample.entity.Student;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface StudentService {

    @WebMethod
    Student getStudent(int id);
}
