package info.hccis.sample.soap;

import info.hccis.sample.entity.Student;
import info.hccis.sample.repositories.StudentRepository;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "info.hccis.sample.soap.StudentService")
public class StudentServiceImpl implements StudentService {
 
    @Inject 
    private StudentRepository _sr;

    public Student getStudent(int id) {
        return _sr.findByStudentId(id);
    }
}